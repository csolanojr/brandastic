<?php

namespace Brandastic\Newsletterdiscount\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

use Magento\Framework\UrlInterface;

class Formurl implements ArgumentInterface
{

    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;

    public function __construct(
        UrlInterface $_urlBuilder
    )
    {
        $this->_urlBuilder = $_urlBuilder;

    }

    public function getUrl(){

        return $this->_urlBuilder->getUrl('newsletter_discount/subscriber/add', ['_secure' => true]);

    }


}
