<?php

namespace Brandastic\Newsletterdiscount\Controller\subscriber;

use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Newsletter\Controller\Subscriber;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Validator\EmailAddress;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Newsletter\Model\Subscriber as SubscriberModel;
use Magento\SalesRule\Api\Data\RuleInterfaceFactory;
use Magento\SalesRule\Api\CouponRepositoryInterface;
use Magento\SalesRule\Model\Coupon\CodegeneratorInterface;
use Magento\SalesRule\Api\Data\CouponInterface;
use Magento\SalesRule\Api\Data\CouponInterfaceFactory;
use Magento\SalesRule\Api\RuleRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\Customer\Api\GroupManagementInterface;
use Magento\Framework\Api\SearchCriteria;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Checkout\Model\Session as CartSession;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\CouponManagementInterface;
use Magento\Quote\Api\CartManagementInterface;
use Brandastic\Newsletterdiscount\Model\Email\Email;
use Magento\Framework\App\ResourceConnection;


class Add extends Subscriber implements HttpPostActionInterface
{

    /**
     * @var EmailAddress
     */
    protected $emailValidator;

    /**
     * @var AccountManagementInterface
     */
    protected $manageCustomer;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var RuleInterfaceFactory
     */
    protected $ruleFactory;

    /**
     * @var CouponInterfaceFactory
     */
    protected $couponFactory;

    /**
     * @var RuleRepositoryInterface
     */
    protected $ruleRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var GroupManagementInterface
     */
    protected $customerGroupManager;

    /**
     * @var WebsiteRepositoryInterface
     */
    protected $websiteRepo;

    /**
     * @var CodegeneratorInterface
     */
    protected $couponGenerator;

    /**
     * @var CouponRepositoryInterface
     */
    protected $couponRepo;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var CartSession
     */
    protected $cartSession;

    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepo;

    /**
     * @var CouponManagementInterface
     */
    protected $quoteCouponInterface;

    /**
     * @var CartManagementInterface
     */
    protected $cartManager;

    /**
     * @var Email
     */
    protected $newsletterEmail;

    /**
     * @var ResourceConnection
     */

    private $resourceConnection;


    /**
     * Add constructor.
     * @param Context $context
     * @param SubscriberFactory $subscriberFactory
     * @param Session $customerSession
     * @param StoreManagerInterface $storeManager
     * @param CustomerUrl $customerUrl
     * @param EmailAddress $emailValidator
     * @param AccountManagementInterface $manageCustomer
     * @param ScopeConfigInterface $scopeConfig
     * @param RuleInterfaceFactory $ruleFactory
     * @param CouponInterfaceFactory $couponFactory
     * @param RuleRepositoryInterface $ruleRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param GroupManagementInterface $customerGroupManager
     * @param WebsiteRepositoryInterface $websiteRepo
     * @param CodegeneratorInterface $couponGenerator
     * @param CouponRepositoryInterface $couponRepo
     * @param LoggerInterface $logger
     * @param CartSession $cartSession
     * @param CartRepositoryInterface $cartRepo
     * @param CouponManagementInterface $quoteCouponInterface
     * @param CartManagementInterface $cartManager
     * @param Email $newsletterEmail
     */
    public function __construct(
        Context $context,
        SubscriberFactory $subscriberFactory,
        Session $customerSession,
        StoreManagerInterface $storeManager,
        CustomerUrl $customerUrl,
        EmailAddress $emailValidator,
        AccountManagementInterface $manageCustomer,
        ScopeConfigInterface $scopeConfig,
        RuleInterfaceFactory $ruleFactory,
        CouponInterfaceFactory $couponFactory,
        RuleRepositoryInterface $ruleRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        GroupManagementInterface $customerGroupManager,
        WebsiteRepositoryInterface $websiteRepo,
        CodegeneratorInterface $couponGenerator,
        CouponRepositoryInterface $couponRepo,
        LoggerInterface $logger,
        CartSession $cartSession,
        CartRepositoryInterface $cartRepo,
        CouponManagementInterface $quoteCouponInterface,
        CartManagementInterface $cartManager,
        Email $newsletterEmail,
        ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
        $this->newsletterEmail = $newsletterEmail;
        $this->cartManager = $cartManager;
        $this->quoteCouponInterface = $quoteCouponInterface;
        $this->cartRepo = $cartRepo;
        $this->cartSession = $cartSession;
        $this->customerSession = $customerSession;
        $this->logger = $logger;
        $this->couponRepo = $couponRepo;
        $this->couponGenerator = $couponGenerator;
        $this->websiteRepo = $websiteRepo;
        $this->customerGroupManager = $customerGroupManager;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->ruleRepository = $ruleRepository;
        $this->couponFactory = $couponFactory;
        $this->ruleFactory = $ruleFactory;
        $this->scopeConfig = $scopeConfig;
        $this->emailValidator = $emailValidator;
        $this->manageCustomer = $manageCustomer;
        parent::__construct($context, $subscriberFactory, $customerSession, $storeManager, $customerUrl);
    }


    /**
     * Validates that the email address isn't being used by a different account.
     *
     * @param string $email
     * @return void
     * @throws LocalizedException
     */
    protected function validateEmailAvailable($email)
    {
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        if ($this->_customerSession->isLoggedIn()
            && ($this->_customerSession->getCustomerDataObject()->getEmail() !== $email
                && !$this->manageCustomer->isEmailAvailable($email, $websiteId))
        ) {
            throw new LocalizedException(
                __('This email address is already assigned to another user.')
            );
        }
    }

    /**
     * Validates that if the current user is a guest, that they can subscribe to a newsletter.
     *
     * @return void
     * @throws LocalizedException
     */
    protected function validateGuestSubscription()
    {
        if ($this->scopeConfig
                ->getValue(SubscriberModel::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG, ScopeInterface::SCOPE_STORE) != 1
            && !$this->_customerSession->isLoggedIn()
        ) {
            throw new LocalizedException(
                __(
                    'Sorry, but the administrator denied subscription for guests. Please <a href="%1">register</a>.',
                    $this->_customerUrl->getRegisterUrl()
                )
            );
        }
    }

    /**
     * Validates the format of the email address
     *
     * @param string $email
     * @return void
     * @throws LocalizedException
     */
    protected function validateEmailFormat($email)
    {
        if (!$this->emailValidator->isValid($email)) {
            throw new LocalizedException(__('Please enter a valid email address.'));
        }
    }


    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function execute()
    {

        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $email = (string)$this->getRequest()->getPost('email');

            try {
                $this->validateEmailFormat($email);
                $this->validateGuestSubscription();
                $this->validateEmailAvailable($email);

                $subscriber = $this->_subscriberFactory->create()->loadByEmail($email);
                if ($subscriber->getId()
                    && (int)$subscriber->getSubscriberStatus() === \Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED
                ) {
                    throw new LocalizedException(
                        __('This email address is already subscribed.')
                    );
                }
                $status = (int)$this->_subscriberFactory->create()->subscribe($email);
                /** Coupon Section */
                $salesRule = $this->getSalesRule();
                $couponCode = $this->generateCoupon();
                $this->saveCoupon($salesRule, $couponCode);
                //$this->addCoupon($couponCode);
                $this->newsletterEmail->sendEmail($email, $couponCode);
                $this->messageManager->addSuccessMessage('You have successfully been added to our newsletter an email with a coupon code has been sent.');

            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong with the subscription.'));
            }
        }
        /** @var \Magento\Framework\Controller\Result\Redirect $redirect */
        $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $redirectUrl = $this->_redirect->getRedirectUrl();
        return $redirect->setUrl($redirectUrl);


    }

    /**
     * @return RuleInterface
     * @throws LocalizedException
     */
    private function getSalesRule(): RuleInterface
    {

        $searchCriteria = $this->createSearchCriteria();
        $rules = $this->ruleRepository->getList($searchCriteria);
        if ($rules->getTotalCount() > 0) {
            foreach ( $rules->getItems() as $rule) {
                return $this->ruleRepository->getById($rule->getRuleId());
            }
        } else {
            /** @var RuleInterface $newRule */
            $newRule = $this->ruleFactory->create();
            $newRule->setName('Newsletter Coupon')
                ->setCouponType("2")
                ->setUseAutoGeneration((bool)1)
                ->setFromDate(date("Y-m-d"))
                ->setIsActive(1)
                ->setIsAdvanced(1)
                ->setSimpleAction('cart_fixed')
                ->setDiscountAmount(20.0000)
                ->setIsRss(0)
                ->setUsesPerCoupon(1)
                ->setCustomerGroupIds($this->getCustomerGroups())
                ->setWebsiteIds($this->getWebsiteIds());

            $extensionAttributes = $newRule->getExtensionAttributes();
            $extensionAttributes->setNewsletterRule(1);
            $newRule->setExtensionAttributes($extensionAttributes);

            try {
                $newRule = $this->ruleRepository->save($newRule);
                $this->salesRuleSaveWorkaround($newRule->getRuleId());

            } catch (InputException $e) {
                $this->logger->error('An issue has occurred creating the Sales Rule');
            } catch (NoSuchEntityException $e) {
                $this->logger->error('An issue has occurred creating the Sales Rule');
            } catch (LocalizedException $e) {
                $this->logger->error('An issue has occurred creating the Sales Rule');
            }

            return $newRule;


        }

    }

    /**
     * @return SearchCriteria
     */
    private function createSearchCriteria(): SearchCriteria
    {
        $filter = $this->filterBuilder
            ->setField('newsletter_rule')
            ->setConditionType('eq')
            ->setValue(1)
            ->create();
        $this->searchCriteriaBuilder->addFilters([$filter]);
        $this->searchCriteriaBuilder->setPageSize(1);
        return $this->searchCriteriaBuilder->create();

    }

    /**
     * @return array
     * @throws LocalizedException
     */
    private function getCustomerGroups(): array
    {
        $customerGroupIds = ['0'];
        $customerGroups = $this->customerGroupManager->getLoggedInGroups();
        foreach ($customerGroups as $customerGroup) {
            $customerGroupIds[] = $customerGroup->getId();
        }
        return $customerGroupIds;

    }

    /**
     * @return array
     */
    private function getWebsiteIds(): array
    {
        $websiteIds = [];
        $websites = $this->websiteRepo->getList();
        foreach ($websites as $website) {
            $websiteIds[] = $website->getId();
        }

        return $websiteIds;

    }

    /**
     * @param RuleInterface $salesRule
     * @param $couponCode
     * @return $this
     */
    private function saveCoupon(RuleInterface $salesRule, $couponCode)
    {
        /** @var CouponInterface $coupon */
        $coupon = $this->couponFactory->create();
        $coupon->setRuleId($salesRule->getRuleId())
            ->setCode($couponCode)
            ->setCreatedAt(date("Y-m-d"))
            ->setUsageLimit($salesRule->getUsesPerCoupon())
            ->setType(1);


        try {
            $coupon = $this->couponRepo->save($coupon);
        } catch (InputException $e) {
            $this->logger->error('An issue has occurred saving the coupon code: ' . $couponCode);
        } catch (NoSuchEntityException $e) {
            $this->logger->error('An issue has occurred saving the coupon code: ' . $couponCode);
        } catch (LocalizedException $e) {
            $this->logger->error('An issue has occurred saving the coupon code: ' . $couponCode);
        }
        return $this;


    }

    /**
     * @param $couponCode
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    private function addCoupon($couponCode)
    {
        $cart = $this->cartSession;
        if ($cart->getQuoteId()) {
            $quote = $cart->getQuote();
            $quote->setCouponCode($couponCode);
            $this->cartRepo->save($quote);
        } else {

            $newCartId = $this->cartManager->createEmptyCart();
            $this->quoteCouponInterface->set($newCartId, $couponCode);
            $cart->setQuoteId($newCartId);

        }

    }

    /**
     * @return string
     */
    private function generateCoupon()
    {
        return $this->couponGenerator->generateCode();

    }

    /**
     * Workaround Due to the SalesRule Repo using deprecated methods to save information.
     * @param $ruleId
     * @return $this
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function salesRuleSaveWorkaround($ruleId){
        $connection = $this->resourceConnection->getConnection();
        $table = $this->resourceConnection->getTableName('salesrule');
        $sql = "UPDATE `" . $table . "` SET `newsletter_rule` = 1 , `coupon_type` = 2 WHERE `rule_id` = ".$ruleId."";
        $connection->query($sql);
        $updatedRule = $this->ruleRepository->getById($ruleId);
        $this->ruleRepository->save($updatedRule);

        return $this;

    }

}
